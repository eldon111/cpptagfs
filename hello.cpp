#define FUSE_USE_VERSION  26
   
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <string>
#include "TagIndex.cpp"
#include "Tag.cpp"

const char * hello_str = "Hello World!\n";
const char * hello_path = "/hello";

TagIndex tagIndex;

int hello_getattr(const char *path, struct stat *stbuf)
{
  int res = 0;
  memset(stbuf, 0, sizeof(struct stat));
  if(strcmp(path, "/") == 0) {
	  stbuf->st_mode = S_IFDIR | 0755;
	  stbuf->st_nlink = 2;
  }
  else if(strcmp(path, hello_path) == 0) {
	  stbuf->st_mode = S_IFREG | 0444;
	  stbuf->st_nlink = 1;
	  stbuf->st_size = strlen(hello_str);
  }
  else
	  res = -ENOENT;

  return res;
}

int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
					   off_t offset, struct fuse_file_info *fi)
{
  (void) offset;
  (void) fi;

  if(strcmp(path, "/") != 0)
	  return -ENOENT;

  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);
  filler(buf, hello_path + 1, NULL, 0);

  return 0;
}

int hello_open(const char *path, struct fuse_file_info *fi)
{
  if(strcmp(path, hello_path) != 0)
	  return -ENOENT;

  if((fi->flags & 3) != O_RDONLY)
	  return -EACCES;

  return 0;
}

int hello_read(const char *path, char *buf, size_t size, off_t offset,
					struct fuse_file_info *fi)
{
  size_t len;
  (void) fi;
  if(strcmp(path, hello_path) != 0)
	  return -ENOENT;

  len = strlen(hello_str);
  if ((unsigned int) offset < len) {
	  if (offset + size > len)
		  size = len - offset;
	  memcpy(buf, hello_str + offset, size);
  } else
	  size = 0;

  return size;
}

struct fuse_operations hello_oper;

/* = {
  .getattr   = hello_getattr,
  .readdir = hello_readdir,
  .open   = hello_open,
  .read   = hello_read,
};*/


int main(int argc, char *argv[])
{
  hello_oper.getattr = hello_getattr;
  hello_oper.readdir = hello_readdir;
  hello_oper.open = hello_open;
  hello_oper.read = hello_read;
  return fuse_main(argc, argv, &hello_oper, NULL);
}
