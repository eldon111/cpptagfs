#ifndef TAG_C
#define TAG_C

#include <string.h>
#include <unordered_set>

class Tag {

	std::string name;
	bool is_File;
	std::unordered_set<std::string> refs;
	std::unordered_set<std::string>::iterator it;
	
public:	
	Tag(std::string name, bool isFile) {
		this->name = name;
		this->is_File = isFile;
	}

	Tag(std::string name) {
		Tag(name, false);
	}

	std::string getName() {
		return this->name;
	}

	void setIsFile(bool isFile) {
		this->is_File = isFile;
	}

	bool isFile() {
		return this->is_File;
	}

	void addReference(std::string ref) {
		refs.insert(ref);
	}

	void removeReference(std::string ref) {
		refs.erase(ref);
	}

	bool containsReference(std::string ref) {
		return refs.count(ref) != 0;
	}

	std::vector<std::string> getReferences() {
		std::vector<std::string> retVal (refs.begin(), refs.end());
		//for (std::vector<char *>::size_type i = 0; i != retVal
		//return refs.toArray(new char[][] {});
		return retVal;
	}

	std::unordered_set<std::string> & getRefs() {
		return refs;
	}
};
#endif
