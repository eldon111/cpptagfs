#define FUSE_USE_VERSION  26
   
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <vector>
#include <sstream>
#include "TagIndex.cpp"
#include "Tag.cpp"

TagIndex tagIndex;

std::string mountPath;
std::string filesPath;

std::vector<std::string>::iterator vit;
std::unordered_set<std::string>::iterator sit;

static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


static std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

static std::string getFilename(const char * path) {
	std::string ret (path);
	size_t pos = ret.rfind('/');
	return ret.substr(pos + 1);
}

static std::vector<std::string> getTagNamesFromPath(const char * path) {
	std::vector<std::string> retVal;
	std::string strPath (path);
	
	if (strPath.compare("/") != 0) {
		return split(strPath.substr(1), '/', retVal);
	}
	return retVal;
}

static std::vector<std::string> getFolderTagNamesFromPath(const char * path) {
	std::vector<std::string> v = getTagNamesFromPath(path);
	v.pop_back();
	return v;
}

static int
tagfs_getattr(const char *path, struct stat *stbuf)
{
	std::cout << "GetAttr: " << path << std::endl;

	if (path != NULL) {

		if (strstr(path, "???") != NULL) {
			tagIndex.printInfo();
			return -ENOENT;
		}

		if (strcmp(path, "/") == 0) {
			int res = lstat(filesPath.c_str(), stbuf);
			if (res == -1) {
				return -errno;
			}
			return 0;
		}
		
		std::string filename = getFilename(path);
		
		if (!tagIndex.containsTag(filename)) {
			std::cout << "    Tag does not exist yet" << std::endl;

			return -ENOENT;
		}
		
		if (tagIndex.getTag(filename).isFile()) {
			std::cout << "  This is a file" << std::endl;

			std::vector<std::string> tagNames = getFolderTagNamesFromPath(path);
			for (vit = tagNames.begin(); vit != tagNames.end(); ++vit) {
				if (!tagIndex.getTag(filename).containsReference(*vit))
					return -ENOENT;
			}
		} else {
			std::cout << "This is a dir" << std::endl;
		}
		
		int res = lstat((filesPath + "/" + filename).c_str(), stbuf);
		if (res == -1) {
			return -errno;
		}
		return 0;
	}

	return -ENOENT;
}

static int
tagfs_fgetattr(const char *path, struct stat *stbuf,
				  struct fuse_file_info *fi)
{
	std::cout << "fgetattr" << std:: endl;
	
	int res;

	(void)path;

	res = fstat(fi->fh, stbuf);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_readlink(const char *path, char *buf, size_t size)
{
	int res;

	res = readlink(path, buf, size - 1);
	if (res == -1) {
		return -errno;
	}

	buf[res] = '\0';

	return 0;
}

struct tagfs_dirp {
	DIR *dp;
	struct dirent *entry;
	off_t offset;
};

static int
tagfs_opendir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static inline struct tagfs_dirp *
get_dirp(struct fuse_file_info *fi)
{
	return (struct tagfs_dirp *)(uintptr_t)fi->fh;
}

static int
tagfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
				 off_t offset, struct fuse_file_info *fi)
{
	std::cout << "ReadDir : " << path << std::endl;

	if (path == NULL) {
		return -ENOENT;
	}

	std::vector<std::string> dirNames = getTagNamesFromPath(path);
	std::unordered_set<std::string> fileNames;

	if (dirNames.size() == 0) { // root dir
		std::vector<std::string> tagNames = tagIndex.getAllTagNames();
		for (vit = tagNames.begin(); vit != tagNames.end(); ++vit) {
			filler(buf, (*vit).c_str(), NULL, 0);
		}
		return 0;
	} else {
		std::unordered_set<std::string> refs = tagIndex.getTag(dirNames[0]).getRefs();
		fileNames.insert(refs.begin(), refs.end());

		for (vit = dirNames.begin(); vit != dirNames.end(); ++vit) {
			sit = fileNames.begin();
			while (sit != fileNames.end()) {
				if (!tagIndex.getTag(*vit).containsReference(*sit)) {
					dirNames.erase(vit);
				}
				sit++;
			}
			if (fileNames.empty()) {
				return 0;
			}
		}
	}

	// get directories to display
	std::unordered_set<std::string> dirs;
	for (sit = fileNames.begin(); sit != fileNames.end(); ++sit) {
		std::unordered_set<std::string> refs = tagIndex.getTag(*sit).getRefs();
		dirs.insert(refs.begin(), refs.end());
	}
	for (vit = dirNames.begin(); vit != dirNames.end(); ++vit) {// remove tags we are already under
		dirs.erase(*vit);
	}

	for (sit = dirs.begin(); sit != dirs.end(); ++sit) {
		filler(buf, (*sit).c_str(), NULL, 0);
	}
	for (sit = fileNames.begin(); sit != fileNames.end(); ++sit) {
		filler(buf, (*sit).c_str(), NULL, 0);
	}

	return 0;	
}

static int
tagfs_releasedir(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static int
tagfs_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;
	
	std::string file = filesPath + "/" + getFilename(path);

	if (S_ISFIFO(mode)) {
		res = mkfifo(file.c_str(), mode);
	} else {
		res = mknod(file.c_str(), mode, rdev);
	}

	if (res == -1) {
		return -errno;
	}
	
	tagIndex.addTag(getFilename(path), true);

	return 0;
}

static int
tagfs_mkdir(const char *path, mode_t mode)
{
	std::cout << "mkdir" << std::endl;
	
	int res;
	
	std::string newTag = getFilename(path);
	tagIndex.addTag(newTag);
	
	res = mkdir((filesPath + "/" + newTag).c_str(), mode);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_unlink(const char *path)
{
	std::cout << "unlink" << std::endl;
	std::string filename = getFilename(path);
	std::unordered_set<std::string> refs = tagIndex.getTag(filename).getRefs();
	for (sit = refs.begin(); sit != refs.end(); ++sit) {
		tagIndex.getTag(*sit).removeReference(filename);
	}
	tagIndex.removeTag(filename);
	
	int res;

	res = unlink((filesPath + "/" + filename).c_str());
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_rmdir(const char *path)
{
	std::cout << "rmdir : " << path << std::endl;

	tagIndex.removeTag(getFilename(path));

	int res;

	res = rmdir((filesPath + "/" + getFilename(path)).c_str());
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_symlink(const char *from, const char *to)
{
	/*int res;

	res = symlink(from, to);
	if (res == -1) {
		return -errno;
	}*/

	return 0;
}

static int
tagfs_rename(const char *from, const char *to)
{
	int res;
	std::string redirFrom (filesPath + "/" + getFilename(from));
	std::string redirTo (filesPath + "/" + getFilename(to));

	res = rename(redirFrom.c_str(), redirTo.c_str());
	if (res == -1) {
		return -errno;
	}
	
	Tag & tagTo = tagIndex.addTag(getFilename(to));
	Tag & tagFrom = tagIndex.getTag(getFilename(from));
	
	tagTo.getRefs().insert(tagFrom.getRefs().begin(), tagFrom.getRefs().end());
	for (sit = tagTo.getRefs().begin(); sit != tagTo.getRefs().end(); ++sit) {
		tagIndex.getTag(*sit).removeReference(tagTo.getName());
	}
	tagIndex.removeTag(tagFrom);

	return 0;
}

static int
tagfs_link(const char *from, const char *to)
{
	/*int res;

	res = link(from, to);
	if (res == -1) {
		return -errno;
	}*/

	return 0;
}

static int
tagfs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
	std::cout << "Create" << std::endl;

	std::string filename = getFilename(path);
	std::string redir = filesPath + "/" + filename;

	if (tagIndex.containsTag(filename)) {
	} else {
		tagIndex.addTag(filename, true);
	}
	Tag & newTag = tagIndex.getTag(filename);
	std::cout << "\t" + newTag.getName() << std::endl;
	
	std::vector<std::string> tagRefs = getFolderTagNamesFromPath(path);
	for (vit = tagRefs.begin(); vit != tagRefs.end(); ++vit) {
		std::cout << "\tadd to tag: " << *vit << std::endl;
		newTag.addReference(*vit);
		tagIndex.getTag(*vit).addReference(newTag.getName());
	}
	
	int fd;
	fd = open(redir.c_str(), fi->flags, mode);
	if (fd == -1) {
		return -errno;
	}

	fi->fh = fd;
	return 0;
}

static int
tagfs_open(const char *path, struct fuse_file_info *fi)
{
	int fd;
	std::string redir = filesPath + "/" + getFilename(path);

	fd = open(redir.c_str(), fi->flags);
	if (fd == -1) {
		return -errno;
	}

	fi->fh = fd;
	return 0;
}

static int
tagfs_read(const char *path, char *buf, size_t size, off_t offset,
			  struct fuse_file_info *fi)
{
	int res;

	(void)path;
	res = pread(fi->fh, buf, size, offset);
	if (res == -1) {
		res = -errno;
	}

	return res;
}

static int
tagfs_write(const char *path, const char *buf, size_t size,
			   off_t offset, struct fuse_file_info *fi)
{
	int res;

	(void)path;

	res = pwrite(fi->fh, buf, size, offset);
	if (res == -1) {
		res = -errno;
	}

	return res;
}

static int
tagfs_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
	std::string redir = filesPath + "/" + getFilename(path);

	res = statvfs(redir.c_str(), stbuf);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_flush(const char *path, struct fuse_file_info *fi)
{
	int res;

	(void)path;

	res = close(dup(fi->fh));
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_release(const char *path, struct fuse_file_info *fi)
{
	(void)path;

	close(fi->fh);

	return 0;
}

static int
tagfs_fsync(const char *path, int isdatasync, struct fuse_file_info *fi)
{
	int res;

	(void)path;

	(void)isdatasync;

	res = fsync(fi->fh);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int
tagfs_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
	return 0;
}

static int
tagfs_getxattr(const char *path, const char *name, char *value, size_t size)
{
	return 0;
}

static int
tagfs_listxattr(const char *path, char *list, size_t size)
{
	return 0;
}

static int
tagfs_removexattr(const char *path, const char *name)
{
	return 0;
}

void *
tagfs_init(struct fuse_conn_info *conn)
{
	return NULL;
}

void
tagfs_destroy(void *userdata)
{
	/* nothing */
}

struct fuse_operations tagfs_oper;

int main(int argc, char *argv[])
{
	if (argc < 3) {
		return 0;
	} else {
		mountPath = argv[1];
		filesPath = argv[argc - 1];
	}
	/*mountPath = argv[1];
	filesPath = "/home/eldon/files";*/
	
	tagfs_oper.init        = tagfs_init;
	tagfs_oper.destroy     = tagfs_destroy;
	tagfs_oper.getattr     = tagfs_getattr;
	tagfs_oper.fgetattr    = tagfs_fgetattr;
	//tagfs_oper.access      = tagfs_access;
	tagfs_oper.readlink    = tagfs_readlink;
	tagfs_oper.opendir     = tagfs_opendir;
	tagfs_oper.readdir     = tagfs_readdir;
	tagfs_oper.releasedir  = tagfs_releasedir;
	tagfs_oper.mknod       = tagfs_mknod;
	tagfs_oper.mkdir       = tagfs_mkdir;
	tagfs_oper.symlink     = tagfs_symlink;
	tagfs_oper.unlink      = tagfs_unlink;
	tagfs_oper.rmdir       = tagfs_rmdir;
	tagfs_oper.rename      = tagfs_rename;
	tagfs_oper.link        = tagfs_link;
	tagfs_oper.create      = tagfs_create;
	tagfs_oper.open        = tagfs_open;
	tagfs_oper.read        = tagfs_read;
	tagfs_oper.write       = tagfs_write;
	tagfs_oper.statfs      = tagfs_statfs;
	tagfs_oper.flush       = tagfs_flush;
	tagfs_oper.release     = tagfs_release;
	tagfs_oper.fsync       = tagfs_fsync;
	tagfs_oper.setxattr    = tagfs_setxattr;
	tagfs_oper.getxattr    = tagfs_getxattr;
	tagfs_oper.listxattr   = tagfs_listxattr;
	tagfs_oper.removexattr = tagfs_removexattr;
	
	return fuse_main(argc - 1, argv, &tagfs_oper, NULL);
}
