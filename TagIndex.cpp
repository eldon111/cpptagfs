#ifndef TAGINDEX_C
#define TAGINDEX_C

#include <iostream>
#include <string.h>
#include <unordered_map>
#include <vector>
#include "Tag.cpp"

class TagIndex {

	std::unordered_map<std::string, Tag> tags;
	std::unordered_map<std::string, Tag>::iterator it;

public:
	Tag & addTag(std::string tagName) {
		return addTag(tagName, false);
	}

	Tag & addTag(std::string tagName, bool isFile) {
		tags.insert ( std::pair<std::string, Tag>(tagName, Tag (tagName, isFile)) );
		return (*tags.find(tagName)).second;
	}
	
	void removeTag(Tag & tag) {
		removeTag(tag.getName());
	}

	void removeTag(std::string tagName) {		
		for (it = tags.begin(); it != tags.end(); it++) {
			(*it).second.removeReference(tagName);
		}
		tags.erase(tagName);
	}

	bool containsTag(std::string tagName) {
		return tags.count(tagName) != 0;
	}

	Tag & getTag(std::string tagName) {
		return tags.at(tagName);
	}

	std::vector<Tag> getTags(std::vector<std::string> tagNames) {
		std::vector<Tag> retVal;
		for (std::vector<std::string>::size_type i = 0; i != tagNames.size(); i++) {
			retVal.push_back((*tags.find(tagNames[i])).second); //tags[tagNames[i]];
		}
		return retVal;
	}

	std::vector<std::string> getAllTagNames() {
		std::vector<std::string> retVal;
		for (it = tags.begin(); it != tags.end(); it++) {
			retVal.push_back((*it).first);
		}
		return retVal;
	}

	std::vector<std::string> findReferencesTo(std::string tagName) {
		// List<Tag> tagList = new ArrayList<Tag>();
		// Tag tagToFind = getTag(tagName);
		// for (Tag t : tags.values()) {
		// if (t.containsReference(tagToFind))
		// tagList.add(t);
		// }
		// return (Tag[]) tagList.toArray();
		return getTag(tagName).getReferences();
	}

	void printInfo() {
		for (it = tags.begin(); it != tags.end(); ++it) {
			std::cout << (*it).second.getName() << std::endl;
			for (std::unordered_set<std::string>::iterator it2 = (*it).second.getRefs().begin(); it2 != (*it).second.getRefs().end(); ++it2) {
				std::cout << "\tRef: " << *it2 << std::endl;
			}
		}
	}
};
#endif
